package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"strconv"
)

func isFileExist(path string) (bool, error) {
	fileInfo, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false, nil
	}
	//我这里判断了如果是0也算不存在
	if fileInfo.Size() == 0 {
		return false, nil
	}
	if err == nil {
		return true, nil
	}
	return false, err
}

func mkdir(filedir string) {
	bool, _ := isFileExist(filedir)
	if bool {
		log.Println("目录已存在")
	} else {
		os.Mkdir(filedir, os.ModePerm)
		log.Println("工作目录已创建")
	}
}

func main() {
	// 定义启动的 web 服务器的端口
	var port int
	// 定义下载的 webserver 的 url/lujing
	var lujing string
	// 指定下载的文件目录
	var filedir string

	flag.IntVar(&port, "port", 8080, "默认 webserver 的端口是 8080！")
	flag.StringVar(&lujing, "lujing", "/download/", "默认的下载子目录是 http://server:port/download")
	flag.StringVar(&filedir, "filedir", "/tmp/remoteDownLoadImage/images/", "指定下载文件的目录 /tmp/remoteDownLoadImage/images/")
	flag.Parse()

	log.Println(filedir)
	// mkdir
	mkdir(filedir)

	//static file handler.
	http.Handle(lujing, http.StripPrefix(lujing, http.FileServer(http.Dir(filedir))))

	log.Println("端口监听：", port)
	//Listen on port 8080
	http.ListenAndServe(":"+strconv.Itoa(port), nil)
}